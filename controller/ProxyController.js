"use strict";

var request = require('request');

class ProxyController {
    Get(req, res) {
        //console.log(req.params[0]);
        
        request.get(req.params[0]).pipe(res);
    }
}

module.exports = ProxyController;