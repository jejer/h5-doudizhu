"use strict";

var uuid = require('node-uuid');

var {Table,TableState,TableError} = require('../model/Table.js');
var {Player,PlayerState} = require('../model/Player.js');
var CardController = require('./CardController');
var PlayerInfo = require('../model/PlayerInfo.js');

class TableController {
    constructor(redis, io) {
        this.redis = redis;
        this.io = io;
    }
    
    New(req, res) {
        let self = this;
        
        // Generate Random Table ID
        let table_id = uuid.v4();
        
        // Init Redis Table Data
        let table = new Table(table_id);
        table.state = TableState.WAIT_PLAYERS;
        this.redis.set(table_id, JSON.stringify(table));
        
        // Init socket.io namespace
        let nsp = this.io.of('/' + table_id);
        this.nsp = nsp;
        nsp.on('connect', function(socket){
            if (!socket.request.headers.cookie || !("open_id" in socket.request.headers.cookie)) {
                return socket.emit('data', {action:"StateChange", success:false, code:TableError.INVALID_OPENID});
                return;
            }
            console.log("CONNECT " + socket.id + " OPEN_ID:"+socket.request.headers.cookie.open_id);
            
            socket.on('disconnect', () => {
                console.log("DISCONNECT "+socket.id);
                
                // Handle User Disconnect
                self.redis.get(socket.id, (err, table_id) => {
                    if (!table_id) {
                        return;
                    }
                    self.redis.get(table_id, (err, reply) => {
                        if (!reply) {
                            return;
                        }
                        let table = JSON.parse(reply);
                        let open_id = "";
                        
                        Object.keys(table.players).forEach( (key) => {
                            var player = table.players[key];
                            
                            if (player.socket_id == socket.id) {
                                player.socket_id = null;
                                player.state = PlayerState.OFFLINE;
                                open_id = player.open_id;
                                
                                self.nsp.emit('data', {action: 'Disconnect', open_id: open_id, socket_id: socket.id});
                            }
                        });

                        self.redis.set(table.table_id, JSON.stringify(table));
                        self.redis.del(socket.id);
                    });
                });
            });
            
            socket.on('data', (data) => {
                console.log('ACTION FROM ' + socket.id + " DATA " + data);
                
                // Handle user action
                var data = JSON.parse(data);
                
                // Check Actions
                if (['Join', 'Ready', 'Bid', 'Play'].indexOf(data.action) == -1) {
                    return socket.emit('data', {action:data.action, success:false, code:TableError.INVALID_ACTION});
                }
                
                self[data.action](socket, data);
            });
        });

        // Return Table ID
        res.json({success: true, table_id: table_id});
    }

    // -----------------SOCKET IO----------------------------------
    Join(socket, data) {
        const {socket_id, table_id, open_id} = TableController._GetBaseInfo(socket, data);

        this.redis.get(table_id, (err, reply) => {
            // Table not exist
            if (!reply) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_NOT_EXIST});
            }
            let table = JSON.parse(reply);
            
            let myseat = -1;
            let player = null;
            
            // Check reconnect
            Object.keys(table.players).forEach( (key) => {
                let _player = table.players[key];
                
                if (_player.open_id == open_id) {
                    _player.socket_id = socket_id;
                    _player.state = PlayerState.ONLINE;
                    myseat = key;
                    
                    player = _player;
                }
            });

            // New Join
            if (myseat == -1) {
                // Check full
                if (table.state != TableState.WAIT_PLAYERS) {
                    return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_FULL});
                }
                
                player = new Player(open_id, socket.id);
                PlayerInfo.where({openid: open_id}).fetch().then((p) => {
                    if (p) {
                        // found
                        player.username = p.get("nickname");
                        player.avatar = p.get("headimgurl");
                    }
                }).finally(() => {
                    myseat = player.JoinTable(table);
                
                    // Change to wait for ready if needed
                    if (Object.keys(table.players).length == 3) {
                        table.state = TableState.WAIT_READY;
                    }
                    
                    // Save Table
                    this.redis.set(table.table_id, JSON.stringify(table));
                    
                    // Save socket id to table map
                    this.redis.set(socket.id, table.table_id);
                    
                    // Notify Players
                    socket.emit('data', {action:data.action, success:true, myseat:myseat, table:Table.UserView(open_id, table)});
                    
                    let clone = Object.assign({}, player);
                    clone.cards = player.cards.length;
                    this.nsp.emit('data', {action:data.action, success:true, seat:myseat, player:clone});
                });
            
                return;
            }
            
            // Save Table
            this.redis.set(table.table_id, JSON.stringify(table));
            
            // Save socket id to table map
            this.redis.set(socket.id, table.table_id);
            
            // Notify Players
            socket.emit('data', {action:data.action, success:true, myseat:myseat, table:Table.UserView(open_id, table)});
            
            let clone = Object.assign({}, player);
            clone.cards = player.cards.length;
            this.nsp.emit('data', {action:data.action, success:true, seat:myseat, player:clone});

            
            // // emit to this socket
            // socket.emit("data", "test");
            // // emit to all other socket except this
            // socket.broadcast.emit('data', 'test2');
            // // emit to all socket in namespace
            // this.nsp.emit('data', 'test3');
            // // emit to specific socket in namespace
            // this.nsp.to(socket.id).emit('data', 'test4');
        });
    }
    
    Ready(socket, data) {
        const {socket_id, table_id, open_id, myseat} = TableController._GetBaseInfo(socket, data);

        this.redis.get(table_id, (err, reply) => {
            // Table not exist
            if (!reply) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_NOT_EXIST});
            }
            let table = JSON.parse(reply);
            
            // Check
            if (table.state != TableState.WAIT_READY && table.state != TableState.WAIT_PLAYERS) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_STATE_MISMATCH});
            }
            let player = table.players[myseat];
            if (player.socket_id != socket_id) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.SEAT_SOCKET_MISMATCH});
            }
            
            // Change Table
            player.state = PlayerState.READY;

            // Notify
            this.nsp.emit('data', {action: data.action, success: true, seat: myseat});
            
            // Deal Cards If All READY
            if (Table.IsAllReady(table)) {
                let cards = CardController.DealCards();

                table.players[0].cards = cards[0];
                this.nsp.to(table.players[0].socket_id).emit('data', {action: 'DealCards', cards: table.players[0].cards});
                table.players[1].cards = cards[1];
                this.nsp.to(table.players[1].socket_id).emit('data', {action: 'DealCards', cards: table.players[1].cards});
                table.players[2].cards = cards[2];
                this.nsp.to(table.players[2].socket_id).emit('data', {action: 'DealCards', cards: table.players[2].cards});
                
                table.leftover = cards.leftover;
                table.state = TableState.BID;
                
                this.nsp.emit('data', {action: 'StateChange', success: true, state: table.state, turn: table.turn});
            }
            
            // Save Table
            this.redis.set(table.table_id, JSON.stringify(table));
        });
    }
    
    Bid(socket, data) {
        const {socket_id, table_id, open_id, myseat} = TableController._GetBaseInfo(socket, data);
        const bid = data.bid;
        const turn = Table.PreviousPlayerKey(myseat);
        
        this.redis.get(table_id, (err, reply) => {
            // Table not exist
            if (!reply) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_NOT_EXIST});
            }
            let table = JSON.parse(reply);
            
            // Check
            if (table.state != TableState.BID) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_STATE_MISMATCH});
            }
            if (myseat != table.turn) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.NOT_TURN});
            }
            let player = table.players[myseat];
            if (player.socket_id != socket_id) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.SEAT_SOCKET_MISMATCH});
            }
            
            // Change Table
            if (data.bid > table.players[table.landlord].bid) {
                table.landlord = myseat;
            }
            table.players[myseat].bid = bid;
            table.turn = turn;
            
            // Notify
            this.nsp.emit('data', {action:data.action, success: true, seat: myseat, bid: bid, turn:table.turn});
            
            // All Timed
            if (bid == 3 || Table.IsAllBided(table)) {
                table.turn = table.landlord;
                table.state = TableState.PLAY;
                table.players[table.landlord].cards = CardController.Sort(table.players[table.landlord].cards.concat(table.leftover));
                table.ontable.turn = table.landlord;
                //table.ontable.cards = table.leftover;
                
                this.nsp.emit('data', {action: 'StateChange', success: true, state: table.state, turn: table.turn, leftover:table.leftover});
            }
            
            // Save Table
            this.redis.set(table.table_id, JSON.stringify(table));
        });
    }
    
    Play(socket, data) {
        const {socket_id, table_id, open_id, myseat} = TableController._GetBaseInfo(socket, data);
        const cards = data.cards;
        const turn = Table.NextPlayerKey(myseat);

        this.redis.get(table_id, (err, reply) => {
            // Table not exist
            if (!reply) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_NOT_EXIST});
            }
            let table = JSON.parse(reply);
            
            // Check
            if (table.state != TableState.PLAY) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.TABLE_STATE_MISMATCH});
            }
            if (myseat != table.turn) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.NOT_TURN});
            }
            let player = table.players[myseat];
            if (player.socket_id != socket_id) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.SEAT_SOCKET_MISMATCH});
            }
            
            // Check cards in hand
            if (!TableController._CheckCardsInHand(cards, table.players[myseat].cards)) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.CARD_NOT_IN_HAND});
            }
            
            // Check RuleRank
            if (!CardController.Compare(cards, table.ontable, myseat)) {
                return socket.emit('data', {action:data.action, success:false, code:TableError.RULERANK_FAIL});
            }

            // Change Table
            if (cards.length != 0) { // Empty array means PASS
                table.ontable.turn = myseat;
                table.ontable.cards = cards;
            }
            table.turn = turn;
            // Remove played cards
            table.players[myseat].cards = table.players[myseat].cards.filter((lhs) => {
                for (let i = 0; i < cards.length; i++) {
                    if (cards[i].rank == lhs.rank && cards[i].suit == lhs.suit) {
                        return false;
                    }
                }
                return true;
            });
            
            // Notify
            this.nsp.emit('data', {action:data.action, success: true, seat: myseat, cards: cards, turn:table.turn});
            
            // Save Table
            this.redis.set(table.table_id, JSON.stringify(table));
        });
    }
    
    static _GetBaseInfo(socket, data) {
        const myseat = data.myseat;
        const table_id = data.table_id;
        const open_id = socket.request.headers.cookie.open_id;
        const socket_id = socket.id;
        return {socket_id:socket_id, table_id:table_id, open_id:open_id, myseat:myseat};
    }
    
    static _CheckCardsInHand(cards, handcards) {
        let pass = 0;
        for (let i = 0; i < handcards.length; i++) {
            for (let j = 0; j < cards.length; j++) {
                if (cards[j].rank == handcards[i].rank && cards[j].suit == handcards[i].suit) {
                    pass++;
                }
            }
        }
        return (pass == cards.length);
    }
}

module.exports = TableController;