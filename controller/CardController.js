"use strict";

var {Card, SUIT} = require('../model/Card.js');

var CardType = {
    ERROR: -99,
    PASS: -1,
    ONE: 1,
    PAIRS: 2,
    THREE: 3,
    THREE_WITH_ONE: 4,
    THREE_WITH_PAIRS: 5,
    PROGRESSION: 6,
    PROGRESSION_PAIRS: 7,
    PLANE: 8,
    PLANE_WITH_ONE: 9,
    PLANE_WITH_PAIRS: 10,
    FOUR_WITH_TWO: 11,
    FOUR_WITH_TWO_PAIRS: 12,
    BOMB: 13,
    KING_BOMB: 14,
};

class CardController {
    static DealCards() {
        let deck = [
            new Card(3, SUIT.HEART),
            new Card(4, SUIT.HEART),
            new Card(5, SUIT.HEART),
            new Card(6, SUIT.HEART),
            new Card(7, SUIT.HEART),
            new Card(8, SUIT.HEART),
            new Card(9, SUIT.HEART),
            new Card(10, SUIT.HEART),
            new Card(11, SUIT.HEART),
            new Card(12, SUIT.HEART),
            new Card(13, SUIT.HEART),
            new Card(14, SUIT.HEART),
            new Card(15, SUIT.HEART),
            
            new Card(3, SUIT.SPADE),
            new Card(4, SUIT.SPADE),
            new Card(5, SUIT.SPADE),
            new Card(6, SUIT.SPADE),
            new Card(7, SUIT.SPADE),
            new Card(8, SUIT.SPADE),
            new Card(9, SUIT.SPADE),
            new Card(10, SUIT.SPADE),
            new Card(11, SUIT.SPADE),
            new Card(12, SUIT.SPADE),
            new Card(13, SUIT.SPADE),
            new Card(14, SUIT.SPADE),
            new Card(15, SUIT.SPADE),
            
            new Card(3, SUIT.CLUB),
            new Card(4, SUIT.CLUB),
            new Card(5, SUIT.CLUB),
            new Card(6, SUIT.CLUB),
            new Card(7, SUIT.CLUB),
            new Card(8, SUIT.CLUB),
            new Card(9, SUIT.CLUB),
            new Card(10, SUIT.CLUB),
            new Card(11, SUIT.CLUB),
            new Card(12, SUIT.CLUB),
            new Card(13, SUIT.CLUB),
            new Card(14, SUIT.CLUB),
            new Card(15, SUIT.CLUB),
            
            new Card(3, SUIT.DIAMOND),
            new Card(4, SUIT.DIAMOND),
            new Card(5, SUIT.DIAMOND),
            new Card(6, SUIT.DIAMOND),
            new Card(7, SUIT.DIAMOND),
            new Card(8, SUIT.DIAMOND),
            new Card(9, SUIT.DIAMOND),
            new Card(10, SUIT.DIAMOND),
            new Card(11, SUIT.DIAMOND),
            new Card(12, SUIT.DIAMOND),
            new Card(13, SUIT.DIAMOND),
            new Card(14, SUIT.DIAMOND),
            new Card(15, SUIT.DIAMOND),
            
            new Card(16, SUIT.JOKER),
            new Card(17, SUIT.JOKER),
            ];
            
        CardController._shuffle(deck);
        return {0: CardController.Sort(deck.splice(0,17)), 
                1: CardController.Sort(deck.splice(0,17)), 
                2: CardController.Sort(deck.splice(0,17)), 
                leftover: CardController.Sort(deck)};
    }
    
    static CheckRule(cards) {
        return true;
    }
    
    static CheckRank(cards, ontable) {
        return true;
    }
    
    static Sort(cards) {
        return cards.sort((a, b) => {
            if (a.rank != b.rank) {
                return (b.rank - a.rank);
            } else {
                return (a.suit - b.suit);
            }
        });
    }
    
    static _shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
         while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }
    
    static Compare(cards, ontable, myseat) {
        let cards_info = CardController.Analyze(cards);
        if (cards_info.type == CardType.ERROR) {
            return false;
        }

        if (myseat == ontable.turn) {
            if (cards_info.type == CardType.PASS) {
                //到自己的时候不能pass
                return false;
            } 

            //其他都可以
            return true;
        }

        if (cards_info.type == CardType.PASS) {
            // 可以pass
            return true;
        }

        if (cards_info.type == CardType.KING_BOMB) {
            // 可以王炸
            return true;
        }

        let ontable_info = CardController.Analyze(ontable.cards);
        if (cards_info.type == CardType.BOMB) {
            if (ontable_info.type == CardType.KING_BOMB) {
                return false;
            }
            if (ontable_info.type == CardType.BOMB) {
                if (cards_info.rank > ontable_info.rank) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        
        if (cards_info.type != ontable_info.type) {
            return false;
        }

        if (cards_info.rank > ontable_info.rank) {
            return true;
        }

        return false;
    }

    static Analyze(cards) {
        let len = cards.length;
        switch (len) {
        case 0:
            return {'type': CardType.PASS};
        case 1:
            return {'type': CardType.ONE, 'rank': cards[0].rank, 'length': len};
        case 2:
            if(CardController._isPairs(cards))
                return {'type': CardType.PAIRS, 'rank': cards[0].rank, 'length': len};
            else if (CardController._isKingBomb(cards))
                return {'type': CardType.KING_BOMB, 'rank': cards[0].rank, 'length': len};
            else
                return {'type': CardType.ERROR};
        case 3:
            if(CardController._isThree(cards))
                return {'type': CardType.THREE, 'rank': cards[0].rank, 'length': len};
            else
                return {'type': CardType.ERROR};
        case 4:
            if(CardController._isThreeWithOne(cards)){
                return {'type': CardType.THREE_WITH_ONE, 'rank': CardController._getMaxRank(cards, 3), 'length': len};
            } else if (CardController._isBomb(cards)) {
                return {'type': CardType.BOMB, 'rank': cards[0].rank, 'length': len};
            }
            return {'type': CardType.ERROR};
        default:
            if(CardController._isProgression(cards))
                return {'type': CardType.PROGRESSION, 'rank': cards[0].rank, 'length': len};
            else if(CardController._isProgressionPairs(cards))
                return {'type': CardType.PROGRESSION_PAIRS, 'rank': cards[0].rank, 'length': len};
            else if(CardController._isThreeWithPairs(cards))
                return {'type': CardType.THREE_WITH_PAIRS, 'rank': CardController._getMaxRank(cards, 3), 'length': len};
            else if(CardController._isPlane(cards))
                return {'type': CardType.PLANE, 'rank': CardController._getMaxRank(cards, 3), 'length': len};
            else if(CardController._isPlaneWithOne(cards))
                return {'type': CardType.PLANE_WITH_ONE, 'rank': CardController._getMaxRank(cards, 3), 'length': len};
            else if(CardController._isPlaneWithPairs(cards))
                return {'type': CardType.PLANE_WITH_PAIRS, 'rank': CardController._getMaxRank(cards, 3), 'length': len};
            else if(CardController._isFourWithTwo(cards))
                return {'type': CardType.FOUR_WITH_TWO, 'rank': CardController._getMaxRank(cards, 4), 'length': len};
            else if(CardController._isFourWithPairs(cards))
                return {'type': CardType.FOUR_WITH_TWO_PAIRS, 'rank': CardController._getMaxRank(cards, 4), 'length': len};
            else
                return {'type': CardType.ERROR};
        }
    }

    //是否是对子
    static _isPairs(cards) {
        return cards.length == 2 && cards[0].rank === cards[1].rank;
    }
    //是否是三根
    static _isThree(cards) {
        return cards.length == 3 && cards[0].rank === cards[1].rank && cards[1].rank === cards[2].rank;
    }
    //是否是三带一
    static _isThreeWithOne(cards) {
        if(cards.length != 4) return false;
        var c = CardController._rankCount(cards);
        return c.length === 2 && (c[0].count === 3 || c[1].count === 3);
    }
    //是否是三带一对
    static _isThreeWithPairs(cards) {
        if(cards.length != 5) return false;
        var c = CardController._rankCount(cards);
        return c.length === 2 && (c[0].count === 3 || c[1].count === 3);
    }
    //是否是顺子
    static _isProgression(cards) {
        if(cards.length < 5 || cards[0].rank === 15) return false;
        for (var i = 0; i < cards.length; i++) {
            if(i != (cards.length - 1) && (cards[i].rank - 1) != cards[i + 1].rank){
                return false;
            }
        }
        return true;
    }
    //是否是连对
    static _isProgressionPairs(cards) {
        if(cards.length < 6 || cards.length % 2 != 0 || cards[0].rank === 15) return false;
        for (var i = 0; i < cards.length; i += 2) {
            if(i != (cards.length - 2) && (cards[i].rank != cards[i + 1].rank || (cards[i].rank - 1) != cards[i + 2].rank)){
                return false;
            }
        }
        return true;
    }
    //是否是飞机
    static _isPlane(cards) {
        if(cards.length < 6 || cards.length % 3 != 0 || cards[0].rank === 15) return false;
        for (var i = 0; i < cards.length; i += 3) {
            if(i != (cards.length - 3) && (cards[i].rank != cards[i + 1].rank || cards[i].rank != cards[i + 2].rank || (cards[i].rank - 1) != cards[i + 3].rank)){
                return false;
            }
        }
        return true;
    }
    //是否是飞机带单
    static _isPlaneWithOne(cards) {
        if(cards.length < 8 || cards.length % 4 != 0) return false;
        var c = CardController._rankCount(cards),
            threeList = [],
            threeCount = cards.length / 4;
        for (var i = 0; i < c.length; i++) {
            if(c[i].count == 3){
                threeList.push(c[i]);
            }
        }
        if(threeList.length != threeCount || threeList[0].rank === 15){//检测三根数量和不能为2
            return false;
        }
        for (i = 0; i < threeList.length; i++) {//检测三根是否连续
            if(i != threeList.length - 1 && threeList[i].rank - 1 != threeList[i + 1].rank){
                return false;
            }
        }
        return true;
    }
    //是否是飞机带对
    static _isPlaneWithPairs(cards) {
        if(cards.length < 10 || cards.length % 5 != 0) return false;
        var c = CardController._rankCount(cards),
            threeList = [],
            pairsList = [],
            groupCount = cards.length / 5;
        for (var i = 0; i < c.length; i++) {
            if(c[i].count == 3){
                threeList.push(c[i]);
            }
            else if(c[i].count == 2){
                pairsList.push(c[i]);
            } else {
                return false;
            }
        }
        if(threeList.length != groupCount || pairsList.length != groupCount || threeList[0].rank === 15){//检测三根数量和对子数量和不能为2
            return false;
        }
        for (i = 0; i < threeList.length; i++) {//检测三根是否连续
            if(i != threeList.length - 1 && threeList[i].rank - 1 != threeList[i + 1].rank){
                return false;
            }
        }
        return true;
    }
    //是否是四带二
    static _isFourWithTwo(cards) {
        var c = CardController._rankCount(cards);
        if(cards.length != 6 || c.length > 3) return false;
        for (var i = 0; i < c.length; i++) {
            if(c[i].count === 4)
                return true;
        }
        return false;
    }
    //是否是四带两个对
    static _isFourWithPairs(cards) {
        if(cards.length != 8) return false;
        var c = CardController._rankCount(cards);
        if(c.length != 3) return false;
        for (var i = 0; i < c.length; i++) {
            if(c[i].count != 4 && c[i].count != 2)
                return false;
        }
        return true;
    }
    //是否是炸弹
    static _isBomb(cards) {
        return cards.length === 4 && cards[0].rank === cards[1].rank && cards[0].rank === cards[2].rank && cards[0].rank === cards[3].rank;
    }
    //是否是王炸
    static _isKingBomb(cards) {
        return cards.length === 2 && cards[0].type == '0' && cards[1].type == '0';
    }

    static _rankCount(cards) {
        var result = [];
        var addCount = function(result , v) {
            for (var i = 0; i < result.length; i++) {
                if(result[i].rank == v){
                    result[i].count ++;
                    return;
                }
            }
            result.push({'rank': v, 'count': 1});
        };
        for (var i = 0; i < cards.length; i++){
            addCount(result, cards[i].rank);
        }
        return result;
    }

    static _getMaxRank(cards, n) {
        var c = CardController._rankCount(cards);
        var max = 0;
        for (var i = 0; i < c.length; i++) {
            if(c[i].count === n && c[i].rank > max){
                max = c[i].rank;
            }
        }
        return max;
    }
}

module.exports = CardController;