"use strict";

var PlayerState = {
    OFFLINE: 0,
    ONLINE: 1,
    READY: 2,
};

class Player {
    constructor(open_id, socket_id) {
        this.open_id = open_id;
        this.socket_id = socket_id;
        this.username = "用户名";
        this.avatar = "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/46";
        this.state = PlayerState.ONLINE;
        this.cards = [];
        this.bid = 0;   //叫分 1,2,3 
    }
    
    JoinTable(table) {
        let myseat = Object.keys(table.players).length;
        table.players[myseat] = this;
        return myseat;
    }
}

module.exports = {Player:Player, PlayerState:PlayerState};