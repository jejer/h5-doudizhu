"use strict";

var {Player,PlayerState} = require('./Player.js');

var TableState = {
    INIT: 0,
    WAIT_PLAYERS: 1,
    WAIT_READY: 2,
    BID: 3,
    PLAY: 4,
    ERROR: -1,
};

var TableError = {
    CONNECTION_LOST: 1,     // 丢失连接
    INVALID_OPENID: 2,      // 无效的open_id
    INVALID_ACTION: 3,      // 无效的Action
    NOT_TURN: 4,            // 不正确的出牌顺序
    CARD_NOT_IN_HAND: 5,    // 出了不在手里的牌
    RULERANK_FAIL: 6,           // 出牌不符合规则
    
    TABLE_NOT_EXIST: 20,    // 桌子不存在
    TABLE_FULL: 21,         // 桌子已满
    
    TABLE_STATE_MISMATCH: 50,   // Action与桌子当前状态不符合
    SEAT_SOCKET_MISMATCH: 51,   // 座位号与socket_id不符合
    
    UNKNOWN_ERROR: 99,
}

class Table {
    constructor(table_id) {
        this.table_id = table_id;
        this.state = TableState.INIT;
        this.players = {};
        this.turn = Math.floor((Math.random() * 3));
        this.landlord = this.turn;
        this.leftover = [];
        this.ontable = {turn:this.turn, cards:[]};
        this.bid = 1;
    }
    
    static PreviousPlayerKey(current) {
        if (current == 0) {
            return 2;
        } else if (current == 1) {
            return 0;
        } else if (current == 2) {
            return 1;
        }
    }
    static NextPlayerKey(current) {
        if (current == 0) {
            return 1;
        } else if (current == 1) {
            return 2;
        } else if (current == 2) {
            return 0;
        }
    }

    static UserView(open_id, table) {
        let clone = Object.assign({}, table);
        Object.keys(clone.players).forEach( (key) => {
            let player = clone.players[key];
            if (player.open_id != open_id) {
                player.cards = player.cards.length;
            }
        });
        if (clone.state == TableState.BID) {
            clone.leftover = [];
        }
        return clone;
    }
    
    static IsAllReady(table) {
        let readys = 0;
        Object.keys(table.players).forEach( (key) => {
            let player = table.players[key];
            if (player.state == PlayerState.READY) {
                readys += 1;
            }
        });
        
        return readys == 3;
    }
    static IsAllBided(table) {
        let bided = 0;
        Object.keys(table.players).forEach( (key) => {
            let player = table.players[key];
            if (player.bid > 0) {
                bided += 1;
            }
        });
        
        return bided == 3;
    }
}

module.exports = {Table:Table, TableState:TableState, TableError:TableError};