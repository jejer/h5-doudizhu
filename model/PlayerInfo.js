var bookshelf = require('./bookshelf.js');

var PlayerInfo = bookshelf.Model.extend({
  tableName: 'playerinfo',
  hasTimestamps: true,
});

module.exports = PlayerInfo;