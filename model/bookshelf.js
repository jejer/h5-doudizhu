// database
var knex = require('knex')({
  client: 'mysql',
  //debug: true,
  connection: {
    host     : 'localhost',
    user     : 'homestead',
    password : 'secret',
    database : 'homestead',
    charset  : 'utf8'
  }
});
var bookshelf = require('bookshelf')(knex);

module.exports = bookshelf;