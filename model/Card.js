"use strict";

var SUIT = {
    JOKER: 0,    //王
    DIAMOND: 1, //方片
    CLUB: 2,    //梅花
    HEART: 3,   //红桃
    SPADE: 4,   //黑桃
};

class Card {
    constructor (rank, suit) {
        this.rank = rank;
        this.suit = suit;
    }
}

module.exports = {Card:Card, SUIT:SUIT};