var gulp = require('gulp');
var webpack = require('webpack-stream');
var less = require('gulp-less');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

gulp.task('default', ['webpack']);

gulp.task('webpack', function() {
    return gulp.src('./view/index.js')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('./public/dist/'));
});

gulp.task('style', ['compile-bootstrap'], function() {
    return gulp.src('./public/dist/css/*.css')
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest('./public/dist/'));
});

gulp.task('compile-bootstrap', function() {
    return gulp.src('./view/vendor/bootstrap/__main.less')
        .pipe(less({paths: ['./view/vendor/bootstrap/less/']}))
        .pipe(rename('bootstrap.css'))
        .pipe(gulp.dest('./public/dist/css/'))
});
