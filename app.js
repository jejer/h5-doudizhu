"use strict";
const PORT = 8080;

// express framework
var express = require('express');
var app = express();
app.use(require('cookie-parser')());
app.use(require('compression')());

// socket.io
var server = require('http').Server(app);
var io = require('socket.io')(server);
io.use(require('socket.io-cookie'));

// redis service
var redis = require("redis").createClient();

// database
var PlayerInfo = require('./model/PlayerInfo.js');

var tableController = new (require('./controller/TableController.js'))(redis, io);
var proxyController = new (require('./controller/ProxyController.js'))();

app.get('/', function (req, res) {
  res.redirect('/index.html');
});

app.get('/table/new', tableController.New.bind(tableController));


app.get('/avatar/*', proxyController.Get.bind(proxyController));


var OAuth = require('wechat-oauth');
//var wx = new OAuth('wxbca42d4004221261', '01b8d91e994703af06f84627af36906c');
var wx = new OAuth('wx388c87c9ef02182c', '45cafea32fa0d3578d2a7682a23ebf53');


app.get('/index.html', (req, res) => {
    if (req.query.open_id) {
        res.cookie('open_id', req.query.open_id, { expires: new Date(Date.now() + 31622400000), httpOnly: true });
        res.redirect('index.html');
        return;
    }
    
    if (req.query.test) {
        res.cookie('open_id', req.query.open_id, { expires: new Date(Date.now() - 100), httpOnly: true });
        res.redirect('index.html');
        return;
    }
    
    if (!req.cookies.open_id) {
        if (req.query.state) {
            if (req.query.code == "authdeny") {
                res.send("授权失败");
                return;
            } else {
                // step 2
                wx.getAccessToken(req.query.code, function (err, result) {
                    let accessToken = result.data.access_token;
                    let openid = result.data.openid;
                    
                    res.cookie('open_id', openid, { expires: new Date(Date.now() + 31622400000), httpOnly: true });
                    res.redirect('index.html');
                    
                    // step 3 get user info
                    wx.getUser(openid, function (err, result) {
                        let userInfo = result;
                        delete userInfo.privilege;
                        PlayerInfo.where({openid: userInfo.openid}).fetch().then((player) => {
                            if (player) {
                                // found
                                player.set(userInfo).save();
                            } else {
                                // not found
                                new PlayerInfo(userInfo).save();
                            }
                        });
                    });
                });
            }
        } else {
            // step 1
            let full_url = req.protocol + '://' + req.get('host') + req.originalUrl;
            res.redirect(wx.getAuthorizeURL(full_url, 'jejer', 'snsapi_userinfo'));
            //res.redirect(wx.getAuthorizeURLForWebsite(full_url, 'jejer', 'snsapi_userinfo'));
        }

    } else {
        res.sendFile('public/index.html' , { root : __dirname});
    }
});

app.use(express.static('public', { maxAge: 86400000 }));


server.listen(PORT, () => {
    console.log('LISTENING ON PORT ' + PORT);
});
