# Note:
## Server:
* [NodeJS](https://nodejs.org/)
* [socket.io](http://socket.io/)
* redis

## Client:
* [vuejs](http://vuejs.org/)
* [webpack](https://webpack.github.io/)
* [gulp](http://gulpjs.com/)
* vue-router
* pixijs