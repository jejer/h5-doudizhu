var knex = require('knex')({
  client: 'mysql',
  connection: {
    host     : 'localhost',
    user     : 'homestead',
    password : 'secret',
    database : 'homestead',
    charset  : 'utf8'
  }
});

knex.schema.createTableIfNotExists('playerinfo', function (table) {
  table.increments('id').primary();
  table.string('openid');
  table.string('nickname');
  table.integer('sex');
  table.string('language');
  table.string('city');
  table.string('province');
  table.string('country');
  table.string('headimgurl');
  table.timestamps();
}).then(() => {
  console.log('created playerinfo table');
}).catch((err) => {
  console.error(err);
}).finally(() => {
    process.exit();
});



