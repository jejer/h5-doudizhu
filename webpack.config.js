var webpack = require('webpack')

module.exports = {
//   entry: './view/index.js',
  output: {
    //path: './public/dist',
    filename: 'bundle.js'
  },
//   externals: {
//         // require("jquery") is external and available
//         //  on the global var jQuery
//         "jquery": "jQuery"
//   },
  module: {
    loaders: [
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.vue$/, loader: 'vue'},
      {test: /\.js$/, exclude: /node_modules|vue\/dist/, loader: 'babel', query: {
        plugins: ['transform-runtime'],
        presets: ['es2015'],
      }}
    ]
  },
  plugins: [
    // new webpack.optimize.UglifyJsPlugin({minimize: true, compress:{warnings: false}})
  ]
}