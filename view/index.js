var Vue = require('vue')
var VueRouter = require('vue-router')
import { configRouter } from './route-config'
//require('es6-promise').polyfill()

// Additional Global Style
require('./style.css')

// Additional Global JS
//var $ = require('jquery')

// Vue DEBUG 
Vue.config.debug = true

// install router
Vue.use(VueRouter)

// create router
const router = new VueRouter({
  saveScrollPosition: true
})

// configure router
configRouter(router)

// boostrap the app
const App = Vue.extend(require('./app.vue'))
router.start(App, '#app')

// just for debugging
window.router = router