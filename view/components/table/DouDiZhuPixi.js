var TableState = require('../../../model/Table.js').TableState;
var PlayerState = require('../../../model/Player.js').PlayerState;

const STAGE_WIDTH = 320;
const STAGE_HEIGHT = 400;
const STAGE_MARGIN = 2;

const CARD_WIDTH = 50;
const CARD_HEIGHT = 71;
const CARD_H_MARGIN = 5;
const CARD_H_OFFSET = 13;
const CARD_V_OFFSET = 10;
const CARD_STANDUP_OFFSET = 25;

const LEFTOVER_WIDTH = CARD_WIDTH*3 + CARD_H_MARGIN*2;
const PLAYER_AREA_WIDTH = 60;
const PLAYER_AREA_HEIGHT = 320;
const PLAYER_AVATAR = 35;
const PLAYER_ROLE_HEIGHT = 14;
const PLAYER_TURN = 19;
const PLAYER_CARDS_AREA_HEIGHT = CARD_V_OFFSET * 20 + CARD_HEIGHT;
const PLAYER_CARDS_AREA_V_OFFSET = 40;
const MYCARDS_AREA_HEIGHT = CARD_HEIGHT + CARD_STANDUP_OFFSET;
const ONTABLE_AREA_WIDTH = CARD_H_OFFSET * 10 + CARD_WIDTH;
const ONTABLE_AREA_HEIGHT = CARD_HEIGHT * 2 + STAGE_MARGIN;
const BUTTONS_AREA_WIDTH = STAGE_WIDTH - PLAYER_AREA_WIDTH*2 - STAGE_MARGIN*4;
const BUTTONS_AREA_HEIGHT = 60;

const TURN_INDICATOR = 15;
const USER_STATE_FONT_SIZE = 12;

class DouDiZhuPixi {
    constructor(div, ddz) {
        this.ddz = ddz;
        this.renderer = PIXI.autoDetectRenderer(STAGE_WIDTH, STAGE_HEIGHT, {backgroundColor : 0x1099bb});
        div.appendChild(this.renderer.view);
        
        this.stage = new PIXI.Container();
        
        // Leftover Container
        this.leftover_area = new PIXI.Container();
        this.leftover_area.width = LEFTOVER_WIDTH;
        this.leftover_area.height = CARD_HEIGHT;
        this.leftover_area.x = (STAGE_WIDTH - LEFTOVER_WIDTH) / 2;
        this.leftover_area.y = STAGE_MARGIN;
        let left0 = new PIXI.Sprite();
        left0.width = CARD_WIDTH;
        left0.height = CARD_HEIGHT;
        this.leftover_area.addChild(left0);
        let left1 = new PIXI.Sprite();
        left1.width = CARD_WIDTH;
        left1.height = CARD_HEIGHT;
        left1.x = CARD_WIDTH + CARD_H_MARGIN;
        this.leftover_area.addChild(left1);
        let left2 = new PIXI.Sprite();
        left2.width = CARD_WIDTH;
        left2.height = CARD_HEIGHT;
        left2.x = (CARD_WIDTH + CARD_H_MARGIN) * 2;
        this.leftover_area.addChild(left2);
        // let graphics = new PIXI.Graphics();
        // graphics.lineStyle(2, 0xFF0000, 1);
        // graphics.beginFill(0x000000, 0);
        // graphics.drawRect(0, 0, 160, 71);
        // this.leftover_area.addChild(graphics);
        this.leftover_area.visible = false;
        this.stage.addChild(this.leftover_area);
        
        // PrevPlayer Container
        this.prev_area = new PIXI.Container();
        this.prev_area.width = PLAYER_AREA_WIDTH;
        this.prev_area.height = PLAYER_AREA_HEIGHT;
        this.prev_area.x = STAGE_MARGIN;
        this.prev_area.y = STAGE_MARGIN;
        this.stage.addChild(this.prev_area);
        
        this.prevuser_area = new PIXI.Container();
        this.prevuser_area.width = PLAYER_AREA_WIDTH - STAGE_MARGIN * 2;
        this.prevuser_area.height = PLAYER_AREA_HEIGHT - PLAYER_CARDS_AREA_HEIGHT - STAGE_MARGIN;
        this.prevuser_area.x = STAGE_MARGIN;
        this.prevuser_area.y = STAGE_MARGIN;
        this.prev_area.addChild(this.prevuser_area);
        
        this.prevcards_area  = new PIXI.Container();
        this.prevcards_area.width = CARD_WIDTH;
        this.prevcards_area.height = PLAYER_CARDS_AREA_HEIGHT;
        this.prevcards_area.x = CARD_H_MARGIN;
        this.prevcards_area.y = PLAYER_CARDS_AREA_V_OFFSET;
        this.prev_area.addChild(this.prevcards_area);
        
        // NextPlayer Container
        this.next_area = new PIXI.Container();
        this.next_area.width = PLAYER_AREA_WIDTH;
        this.next_area.height = PLAYER_AREA_HEIGHT;
        this.next_area.x = STAGE_WIDTH - STAGE_MARGIN - PLAYER_AREA_WIDTH;
        this.next_area.y = STAGE_MARGIN;
        this.stage.addChild(this.next_area);
        
        this.nextuser_area = new PIXI.Container();
        this.nextuser_area.width = PLAYER_AREA_WIDTH - STAGE_MARGIN * 2;
        this.nextuser_area.height = PLAYER_AREA_HEIGHT - PLAYER_CARDS_AREA_HEIGHT - STAGE_MARGIN;
        this.nextuser_area.x = STAGE_MARGIN;
        this.nextuser_area.y = STAGE_MARGIN;
        this.next_area.addChild(this.nextuser_area);
        
        this.nextcards_area  = new PIXI.Container();
        this.nextcards_area.width = CARD_WIDTH;
        this.nextcards_area.height = PLAYER_CARDS_AREA_HEIGHT;
        this.nextcards_area.x = CARD_H_MARGIN;
        this.nextcards_area.y = PLAYER_CARDS_AREA_V_OFFSET;
        this.next_area.addChild(this.nextcards_area);
        
        // MyCards Container
        this.mycards_area = new PIXI.Container();
        this.mycards_area.width = STAGE_WIDTH - STAGE_MARGIN * 4 - PLAYER_AREA_WIDTH * 2;
        this.mycards_area.height = MYCARDS_AREA_HEIGHT;
        this.mycards_area.x = STAGE_MARGIN;
        this.mycards_area.y = STAGE_HEIGHT - STAGE_MARGIN - MYCARDS_AREA_HEIGHT;
        this.stage.addChild(this.mycards_area);
        this.selected_cards = [];
        
        // Ontable Container
        this.ontable_area = new PIXI.Container();
        this.ontable_area.width = ONTABLE_AREA_WIDTH;
        this.ontable_area.height = ONTABLE_AREA_HEIGHT;
        this.ontable_area.x = PLAYER_AREA_WIDTH + STAGE_MARGIN*2;
        this.ontable_area.y = CARD_HEIGHT + STAGE_MARGIN*2;
        this.stage.addChild(this.ontable_area);
        
        // Buttons Container
        this.buttons_area = new PIXI.Container();
        this.buttons_area.width = BUTTONS_AREA_WIDTH;
        this.buttons_area.height = BUTTONS_AREA_HEIGHT;
        this.buttons_area.x = PLAYER_AREA_WIDTH + STAGE_MARGIN*2;
        this.buttons_area.y = STAGE_HEIGHT - MYCARDS_AREA_HEIGHT - STAGE_MARGIN*2 - BUTTONS_AREA_HEIGHT;
        this.stage.addChild(this.buttons_area);
        this.wait_confirm = false;  // Button action need server confirm
        
        this.info = new PIXI.Text("载入中>>>");
        this.stage.addChild(this.info);
        
        // Load Assets
        this.textures = {};
        let loader = PIXI.loader;
        // https://github.com/englercj/resource-loader/blob/master/src/Loader.js
        if (loader.progress == 100) {
            this._onAssetsLoaded(loader, loader.resources);
        } else if (loader.progress > 0) {
            // We already added tasks
            loader.load();
        } else {
            loader.add('background', '/assets/background.jpg');
            loader.add('poker', '/assets/atlas/poker.json');
            loader.once('complete', this._onAssetsLoaded.bind(this));
            loader.load();
        }
        
        this._animate();
    }
    
    _animate() {
        // start the timer for the next animation loop
        requestAnimationFrame(this._animate.bind(this));

        // this is the main render call that makes pixi draw your container and its children.
        this.renderer.render(this.stage);
    }
    
    _onAssetsLoaded(loader, resources) {
        // Background
        this.stage.addChildAt(new PIXI.Sprite(resources.background.texture), 0);
        
        // Poker
        for (let suit = 1; suit < 5; suit++) {
            for (let rank = 3; rank < 16; rank++) {
                let name = rank+"_"+suit;
                this.textures[name] = resources.poker.textures[name];
            }
        }
        this.textures["16_0"] = resources.poker.textures['16_0'];
        this.textures["17_0"] = resources.poker.textures['17_0'];
        this.textures["bg"] = resources.poker.textures['bg'];

        // turn indicator
        this.turn_indicator = new PIXI.Sprite(resources.poker.textures['turn']);
        this.turn_indicator.width = TURN_INDICATOR;
        this.turn_indicator.height = TURN_INDICATOR;
        this.turn_indicator.visible = false;
        this.stage.addChild(this.turn_indicator);
        
        // Init Leftover
        for (let i = 0; i < 3; i++) {
            let leftover = this.leftover_area.getChildAt(i);
            leftover.texture = this.textures["bg"];
        }
        
        this.ddz.joinTable();
        this.info.text = "连接中>>>";
    }
    
    serverEventHandler(data) {

        if (data.action == 'Join') {
            this._JoinHandler(data);
        } else if (data.action == 'Ready') {
            this._ReadyHandler(data);
        } else if (data.action == 'Bid') {
            this._BidHandler(data);
        } else if (data.action == 'Play') {
            this._PlayHandler(data);
        } else if (data.action == 'Disconnect') {
            this._DisconnectHandler(data);
        } else if (data.action == 'StateChange') {
            if (!data.success) {
                console.log("ERROR: " + data.code);
                this.info.text = "ERROR: " + data.code;
                this.info.visible = true;
                return;
            }
            this._recoverTable();
        }
    }
    
    _JoinHandler(data) {
        if ('myseat' in data) {
            // My Join, Init Table
            this._recoverTable();
            this.info.visible = false;
            return;
        }
        
        if (data.seat == this.ddz.myseat) {
            return;
        }
        
        // update other player info
        if (data.seat == this.ddz.prevseat) {
            this._refreshPlayer(this.prevuser_area, data.seat);
            this._refreshPlayerCards(this.prevcards_area, data.seat);
        } else if (data.seat == this.ddz.nextseat) {
            this._refreshPlayer(this.nextuser_area, data.seat);
            this._refreshPlayerCards(this.nextcards_area, data.seat);
        }
    }
    
    _ReadyHandler(data) {
        if ( (this.ddz.table.state != TableState.WAIT_PLAYERS) &&
             (this.ddz.table.state != TableState.WAIT_READY) ) {
            return;
        }
        
        if (!data.success && this.wait_confirm) {
            // My Ready Fail
            this.wait_confirm = false;
            this._showReadyButton();
        }
        
        if (data.success && data.seat == this.ddz.myseat) {
            // My Ready Done, Show Already Ready
            this.wait_confirm = false;
            this._showReadyButton();
        }
        
        //update other players ready info
        if (data.seat == this.ddz.prevseat) {
            this._refreshPlayer(this.prevuser_area, data.seat);
            this._refreshPlayerCards(this.prevcards_area, data.seat);
        } else if (data.seat == this.ddz.nextseat) {
            this._refreshPlayer(this.nextuser_area, data.seat);
            this._refreshPlayerCards(this.nextcards_area, data.seat);
        }
    }
    
    _BidHandler(data) {
        if (this.ddz.table.state != TableState.BID) {
            return;
        }
        
        if (!data.success && this.wait_confirm) {
            // My Bid Fail
            this.wait_confirm = false;
        }
        
        if (data.success && data.seat == this.ddz.myseat) {
            // My Bid Done, Show My Bid
            this.wait_confirm = false;
        }
        
        //show player bid
        if (data.seat == this.ddz.prevseat) {
            this._refreshPlayer(this.prevuser_area, data.seat);
            this._refreshPlayerCards(this.prevcards_area, data.seat);
        } else if (data.seat == this.ddz.nextseat) {
            this._refreshPlayer(this.nextuser_area, data.seat);
            this._refreshPlayerCards(this.nextcards_area, data.seat);
        }
        
        this._showBidButtons();
        this._refreshTurn();
    }
    
    _PlayHandler(data) {
        if (this.ddz.table.state != TableState.PLAY) {
            return;
        }
        
        if (data.seat == this.ddz.myseat && this.wait_confirm) {
            this.wait_confirm = false;
            this.selected_cards = [];
            this._refreshMyCards();
        }
        
        if (data.success) {
            this._refreshOntable();
            
            if (this.ddz.table.turn != this.ddz.myseat) {
                this.buttons_area.removeChildren();
            }
        }
        
        if (data.success && data.seat != this.ddz.myseat) {
            if (data.seat == this.ddz.prevseat) {
                this._refreshPlayerCards(this.prevcards_area, data.seat);
            } else if (data.seat == this.ddz.nextseat) {
                this._refreshPlayerCards(this.nextcards_area, data.seat);
            }
        }
        
        this._showPlayButtons();
        this._refreshTurn();
    }
    
    _DisconnectHandler(data) {
        this._refreshPlayer(this.prevuser_area, this.ddz.prevseat);
        this._refreshPlayer(this.nextuser_area, this.ddz.nextseat);
    }
    
    _recoverTable() {
        // players
        this._refreshPlayer(this.prevuser_area, this.ddz.prevseat);
        this._refreshPlayer(this.nextuser_area, this.ddz.nextseat);
        
        // player cards
        this._refreshPlayerCards(this.prevcards_area, this.ddz.prevseat);
        this._refreshPlayerCards(this.nextcards_area, this.ddz.nextseat);
        
        // my cards
        this._refreshMyCards();
        
        // leftover
        this._refreshLeftover();
        
        // ontable
        this._refreshOntable();
        
        // state
        this._refreshButtons();
        this._refreshTurn();
    }
    
    _refreshTurn() {
        if (this.ddz.table.state == TableState.WAIT_PLAYERS || 
            this.ddz.table.state == TableState.WAIT_READY) {
            this.turn_indicator.visible = false;
            return;
        }
        
        if (this.ddz.table.turn == this.ddz.prevseat) {
            this.turn_indicator.position = this.prevuser_area.toGlobal({x:0, y:0});
            this.turn_indicator.x = this.turn_indicator.x + PLAYER_AREA_WIDTH - STAGE_MARGIN*2 - TURN_INDICATOR;
            this.turn_indicator.y = this.turn_indicator.y + USER_STATE_FONT_SIZE + STAGE_MARGIN;
        } else if (this.ddz.table.turn == this.ddz.nextseat) {
            this.turn_indicator.position = this.nextuser_area.toGlobal(this.nextuser_area.position);
            this.turn_indicator.x = this.turn_indicator.x + PLAYER_AREA_WIDTH - STAGE_MARGIN*2 - TURN_INDICATOR;
            this.turn_indicator.y = this.turn_indicator.y + USER_STATE_FONT_SIZE + STAGE_MARGIN;
        } else if (this.ddz.table.turn == this.ddz.myseat) {
            this.turn_indicator.x = STAGE_WIDTH - STAGE_MARGIN - TURN_INDICATOR;
            this.turn_indicator.y = STAGE_HEIGHT - STAGE_MARGIN - TURN_INDICATOR;
        }
        this.turn_indicator.visible = true;
    }
    
    _refreshLeftover() {
        if (this.ddz.table.state == TableState.BID) {
            let texture = this.textures["bg"];
            for (let i = 0; i < 3; i++) {
                let leftover = this.leftover_area.getChildAt(i);
                leftover.texture = this.textures["bg"];
            }
            this.leftover_area.visible = true;
        } else if (this.ddz.table.state == TableState.PLAY) {
            for (let i = 0; i < 3; i++) {
                let leftover = this.leftover_area.getChildAt(i);
                let poker = this.ddz.table.leftover[i].rank + "_" + this.ddz.table.leftover[i].suit;
                leftover.texture = this.textures[poker];
            }
            this.leftover_area.visible = true;
        } else {
            this.leftover_area.visible = false;
        }
    }
    
    _refreshPlayer(area, seat) {
        area.removeChildren();
        if (!(seat in this.ddz.table.players)) {
            return;
        }
        
        let loader = new PIXI.loaders.Loader();
        loader.add('avatar', "/avatar/"+this.ddz.table.players[seat].avatar, {loadType: 2});    //hack
        loader.once('complete', (loader, resources) => {
            let avatar = new PIXI.Sprite(resources.avatar.texture);
            avatar.width = PLAYER_AVATAR;
            avatar.height = PLAYER_AVATAR;
            avatar.x = 0;
            avatar.y = 0;
            area.addChild(avatar);
        });
        loader.load();

        // user name
        // let username = new PIXI.Text(this.ddz.table.players[seat].username);
        // username.x = 0;
        // username.y = PLAYER_AVATAR;
        // area.addChild(username);
        
        // role
        let role_text = "";
        if (this.ddz.table.players[seat].state == PlayerState.OFFLINE) {
            role_text = "离线";
        } else {
            if (this.ddz.table.state == TableState.PLAY) {
                if (seat == this.ddz.table.landlord) {
                    role_text = "地主";
                } else {
                    role_text = "农民";
                }
            } else if (this.ddz.table.state == TableState.BID) {
                let bid = this.ddz.table.players[seat].bid;
                if (bid > 0) {
                    role_text = bid+"分";
                }
            } else {
                if (this.ddz.table.players[seat].state == PlayerState.READY) {
                    role_text = "准备";
                }
            }
        }
        let role = new PIXI.Text(role_text, {font : USER_STATE_FONT_SIZE+'px Arial', fill: '#FFFFFF'});
        role.x = PLAYER_AVATAR;
        role.y = 0;
        area.addChild(role);
    }
    
    _refreshPlayerCards(area, seat) {
        area.removeChildren();
        if (!(seat in this.ddz.table.players)) {
            return;
        }
        for (let i = 0; i < this.ddz.table.players[seat].cards; i++) {
            let card = new PIXI.Sprite(this.textures["bg"]);
            card.y = CARD_V_OFFSET * i;
            area.addChild(card);
        }
    }
    
    _refreshMyCards() {
        this.mycards_area.removeChildren();
        this.selected_cards = [];

        for (let i = 0; i < this.ddz.table.players[this.ddz.myseat].cards.length; i++) {
            let poker = this.ddz.table.players[this.ddz.myseat].cards[i].rank + "_" + this.ddz.table.players[this.ddz.myseat].cards[i].suit;
            let card = new PIXI.Sprite(this.textures[poker]);
            card.x = CARD_H_OFFSET * i;
            card.y = CARD_STANDUP_OFFSET;
            card.buttonMode = true;
            card.interactive = true;
            this.mycards_area.addChild(card);
            
            card.click = (event) => {
                let target = event.target;
                let is_selected = !target.y;
                let index = this.mycards_area.getChildIndex(target);
                
                if (!is_selected) {
                    target.y = 0;
                    this.selected_cards.push(index);
                    this.selected_cards.sort((a,b)=>{return a-b});
                } else {
                    target.y = CARD_STANDUP_OFFSET;
                    let offset = this.selected_cards.indexOf(index);
                    this.selected_cards.splice(offset, 1);
                    this.selected_cards.sort((a,b)=>{return a-b});
                }
                
                // Buttons
                this._showPlayButtons();
            };
            card.on("touchend", card.click);
        }
    }
    
    _refreshOntable() {
        this.ontable_area.removeChildren();
        for (let i = 0; i < this.ddz.table.ontable.cards.length; i++) {
            let poker = this.ddz.table.ontable.cards[i].rank + "_" + this.ddz.table.ontable.cards[i].suit;
            let card = new PIXI.Sprite(this.textures[poker]);
            card.x = (i < 10) ? (CARD_H_OFFSET * i) : (CARD_H_OFFSET * (i-10));
            card.y = (i < 10) ? 0 : (CARD_HEIGHT + STAGE_MARGIN);
            this.ontable_area.addChild(card);
        }
    }
    
    _refreshButtons() {
        if ( ( (this.ddz.table.state == TableState.WAIT_PLAYERS)
             || (this.ddz.table.state == TableState.WAIT_READY) )
             && (this.ddz.table.players[this.ddz.myseat].state == PlayerState.ONLINE) ) {
            
            // Show READY button
            this._showReadyButton();
        } else if (this.ddz.table.state == TableState.BID) {
            
            // Show BID buttons
            this._showBidButtons();
        } else if (this.ddz.table.state == TableState.PLAY) {
            
            // Show Play buttons
            this._showPlayButtons();
        }
    }
    
    _showReadyButton() {
        this.buttons_area.removeChildren();
        
        if (this.ddz.table.players[this.ddz.myseat].state == PlayerState.READY) {
            let ready = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "已准备", "DONE", null);
            this.buttons_area.addChild(ready);
            return;
        }
        
        let ready = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "准备", "",
            () => {
                this.wait_confirm = true;
                this.ddz.ready();
                
                this.buttons_area.removeChildren();
                let ready = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "准备", "DISABLE", null);
                this.buttons_area.addChild(ready);
            }
        );
        this.buttons_area.addChild(ready);
    }
    

    _showBidButtons() {
        this.buttons_area.removeChildren();
        
        if (this.ddz.table.players[this.ddz.myseat].bid > 0) {
            let bid = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, 
                this.ddz.table.players[this.ddz.myseat].bid + "分", "DONE", null);
            this.buttons_area.addChild(bid);
            return;
        }
        
        if (this.ddz.table.turn != this.ddz.myseat) {
            return;
        }
        
        let bid3 = this._newButton(0, 0, BUTTONS_AREA_WIDTH/3, BUTTONS_AREA_HEIGHT, "3分", "",
            () => {
                this.wait_confirm = true;
                this.ddz.bid(3);
                
                this.buttons_area.removeChildren();
                let bid = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "3分", "DISABLE", null);
                this.buttons_area.addChild(bid);
            });
        this.buttons_area.addChild(bid3);
        
        let bid2 = this._newButton(BUTTONS_AREA_WIDTH/3, 0, BUTTONS_AREA_WIDTH/3, BUTTONS_AREA_HEIGHT, "2分", "",
            () => {
                this.wait_confirm = true;
                this.ddz.bid(2);
                
                this.buttons_area.removeChildren();
                let bid = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "2分", "DISABLE", null);
                this.buttons_area.addChild(bid);
            });
        this.buttons_area.addChild(bid2);
        
        let bid1 = this._newButton(BUTTONS_AREA_WIDTH-BUTTONS_AREA_WIDTH/3, 0, BUTTONS_AREA_WIDTH/3, BUTTONS_AREA_HEIGHT, "1分", "",
            () => {
                this.wait_confirm = true;
                this.ddz.bid(1);
                
                this.buttons_area.removeChildren();
                let bid = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "1分", "DISABLE", null);
                this.buttons_area.addChild(bid);
            });
        this.buttons_area.addChild(bid1);
    }
    
    _showPlayButtons() {
        this.buttons_area.removeChildren();
        
        if (this.ddz.table.turn != this.ddz.myseat) {
            return;
        }
        
        let play = null;
        if (!this.ddz.checkPlay(this.selected_cards)) {
            play = this._newButton(0, 0, BUTTONS_AREA_WIDTH/2, BUTTONS_AREA_HEIGHT, "出牌", "DISABLE", null);
        } else {
            play = this._newButton(0, 0, BUTTONS_AREA_WIDTH/2, BUTTONS_AREA_HEIGHT, "出牌", "", 
                () => {
                    this.wait_confirm = true;
                    this.ddz.play(this.selected_cards);
                    
                    this.buttons_area.removeChildren();
                    let wait = this._newButton(0, 0, BUTTONS_AREA_WIDTH, BUTTONS_AREA_HEIGHT, "等待", "DISABLE", null);
                    this.buttons_area.addChild(wait);
                }
            );
        }
        this.buttons_area.addChild(play);
        
        let pass = this._newButton(BUTTONS_AREA_WIDTH/2, 0, BUTTONS_AREA_WIDTH/2, BUTTONS_AREA_HEIGHT, "不出", "", 
            () => {
                this.wait_confirm = true;
                this.ddz.play([]);
            }
        );
        this.buttons_area.addChild(pass);
    }
    
    _newButton(x, y, width, height, text, state, cb) {
        let button = new PIXI.Container();
        button.position.x = x;
        button.position.y = y;
        button.width = width;
        button.height = height;
        
        let btn_color = 0x70CCE3;
        let border_color = 0x61B6CA;
        let text_color = 0xFFFFFF;
        if (state == "DONE") {
            btn_color = 0xA5D7D4;
            border_color = 0x93C1BF;
        } else if (state == "DISABLE") {
            btn_color = 0xDBE2CA;
            border_color = 0xC5C9B3;
        } 
        
        let background = new PIXI.Graphics();
        background.lineStyle(2, border_color, 1);
        background.beginFill(btn_color, 1);
        background.drawRoundedRect(STAGE_MARGIN, STAGE_MARGIN, width-STAGE_MARGIN*2, height-STAGE_MARGIN*2, STAGE_MARGIN*2);
        button.addChild(background);

        button.buttonMode = true;
        button.interactive = true;
        button.click = cb;
        button.on("touchend", button.click);
        
        let basicText = new PIXI.Text(text);
        basicText.x = 5;
        basicText.y = 5;
        basicText.style.fill = text_color;
        button.addChild(basicText);
        
        return button;
    }
}


module.exports = DouDiZhuPixi;