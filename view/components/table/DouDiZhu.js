"use strict";

var {Table, TableState, TableError} = require('../../../model/Table.js');
var {Player, PlayerState} = require('../../../model/Player.js');
var CardController = require('../../../controller/CardController.js');

class DouDiZhu {
    constructor(table_id) {
        this.table = new Table(table_id);
        this.error = 0;
        this.myseat = -1;
        this.prevseat = -1;
        this.nextseat = -1;

        this.connectTable();
    }
    
    setServerEventHandler(handler) {
        this.server_event_handler = handler;
    }

    connectTable() {
        var socket = io('/' + this.table.table_id);
        this.socket = socket;
        
        // Connected
        socket.on('connect', ()=>{
            console.log('connected');
            //this.joinTable();
        });
        
        // On Error, disconnected
        socket.on('error', (err)=>{
            console.log('error: '+err);
            if (err == "Invalid namespace") {
                this.table.state = TableState.ERROR;
                this.error = TableError.TABLE_NOT_EXIST;
            } else {
                this.table.state = TableState.ERROR;
                this.error = TableError.UNKNOWN_ERROR;
            }
            
            this.socket.disconnect();
            this.server_event_handler({action:"StateChange", success:false, code: this.error});
        });
        
        // Disconnected
        socket.on('disconnect', ()=>{
            console.log('disconnect');
            this.table.state = TableState.ERROR;
            this.error = TableError.CONNECTION_LOST;
            this.server_event_handler({action:"StateChange", success:false, code: this.error});
        });
        
        socket.on('data', (data) => {
            console.log(data);
            
            if ('turn' in data) {
                this.table.turn = data.turn;
            }
            
            if (data.action == 'Join') {
                this.Join(data);
                if (this.myseat != -1) {
                    this.server_event_handler(data);
                }
            } else if (this.myseat != -1) {
                // Drop data if join not complete
                this[data.action](data);
                this.server_event_handler(data);
            }
        });
        
        socket.on('reconnect', (num)=>{console.log('reconnect retry:'+num)});
        socket.on('reconnect_attempt', ()=>{console.log('reconnect_attempt')});
        socket.on('reconnecting', (num)=>{console.log('reconnecting retry:'+num)});
        socket.on('reconnect_error', (err)=>{console.log('reconnect_error err:'+err)});
        socket.on('reconnect_failed', ()=>{console.log('reconnect_failed')});
    }
    
    Disconnect(data) {
        // Other Player Disconnect 
        Object.keys(this.table.players).forEach( (key) => {
            let player = this.table.players[key];
            if (player.socket_id == data.socket_id) {
                player.state = PlayerState.OFFLINE;
            }
        });
    }
    
    joinTable() {
        var data = {action: 'Join', table_id: this.table.table_id};
        this.socket.emit('data', JSON.stringify(data));
    }
    
    Join(data) {
        if (!data.success) {
            this.table.state = TableState.ERROR;
            this.error = data.code;
            return;
        }
        
        // MAYBE RECONNECT
        if ('myseat' in data) {
            // self join success
            this.myseat = data.myseat;
            this.table = data.table;
            this.prevseat = Table.PreviousPlayerKey(this.myseat);
            this.nextseat = Table.NextPlayerKey(this.myseat);
        } else if (data.seat != this.myseat) {
            this.table.players[data.seat] = data.player;
        }
    }
    
    ready() {
        var data = {action: 'Ready', table_id: this.table.table_id, myseat: this.myseat};
        this.socket.emit('data', JSON.stringify(data));
    }
    
    Ready(data) {
        if (!data.success) {
            return;
        }
        
        this.table.players[data.seat].state = PlayerState.READY;
    }
    
    DealCards(data) {
        this.table.players[this.myseat].cards = data.cards;
    }
    
    StateChange(data) {
        if (!data.success) {
            // Something Error, Server let us stop
            this.table.state = TableState.ERROR;
            this.error = data.code;
            return;
        }
        
        this.table.state = data.state;
        if (data.state == TableState.PLAY) {
            this.table.leftover = data.leftover;
            this.table.ontable.turn = data.turn;
            this.table.landlord = data.turn;
            
            let mycards = this.table.players[this.myseat].cards;
            this.table.players[0].cards = 17;
            this.table.players[1].cards = 17;
            this.table.players[2].cards = 17;
            this.table.players[this.table.landlord].cards = 20;
            
            if (this.myseat == this.table.landlord) {
                mycards = mycards.concat(this.table.leftover);
                this.table.players[this.myseat].cards = CardController.Sort(mycards);
            } else {
                this.table.players[this.myseat].cards = mycards;
            }
        } else if (data.state == TableState.BID) {
            let mycards = this.table.players[this.myseat].cards;
            this.table.players[0].cards = 17;
            this.table.players[1].cards = 17;
            this.table.players[2].cards = 17;
            this.table.players[this.myseat].cards = mycards;
        }
    }
    
    bid(bid) {
        var data = {action: 'Bid', table_id: this.table.table_id, myseat: this.myseat, bid: bid};
        this.socket.emit('data', JSON.stringify(data));
    }
    
    Bid(data) {
        if (!data.success) {
            return;
        }
        this.table.players[data.seat].bid = data.bid;
    }
    
    play(indexs) {
        let cards = [];
        indexs.forEach((i) => {
            cards.push(this.table.players[this.myseat].cards[i]);
        });

        var data = {action: 'Play', table_id: this.table.table_id, myseat: this.myseat, cards: cards};
        this.socket.emit('data', JSON.stringify(data));
    }
    
    Play(data) {
        if (!data.success) {
            return;
        }
        
        if (data.cards.length == 0) {
            //User Pass
            return;
        }
        
        this.table.ontable.cards = data.cards;
        this.table.ontable.turn = data.seat;
        
        
        // Remove played cards in hand
        if (data.seat == this.myseat) {
            this.table.players[this.myseat].cards = this.table.players[this.myseat].cards.filter((lhs) => {
                for (let i = 0; i < data.cards.length; i++) {
                    if (data.cards[i].rank == lhs.rank && data.cards[i].suit == lhs.suit) {
                        return false;
                    }
                }
                return true;
            });
        } else {
            this.table.players[data.seat].cards -= data.cards.length;
        }
    }
    
    checkPlay(indexs) {
        if (indexs.length === 0) {
            return false;
        }
        
        let cards = [];
        indexs.forEach((i) => {
            cards.push(this.table.players[this.myseat].cards[i]);
        });
        
        return CardController.Compare(cards, this.table.ontable, this.myseat);
    }
}

module.exports = DouDiZhu;